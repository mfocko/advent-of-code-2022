use std::collections::{BTreeSet, VecDeque};

use aoc_2022::*;

type Input = Vec<Vec<char>>;
type Output = usize;

type Position = Vector2D<isize>;

fn find(map: &[Vec<char>], expected: char) -> Option<Position> {
    for (y, row) in map.iter().enumerate() {
        for (x, &c) in row.iter().enumerate() {
            if c == expected {
                return Some(Position::new(x as isize, y as isize));
            }
        }
    }

    None
}

fn find_start(map: &[Vec<char>]) -> Position {
    if let Some(pos) = find(map, 'S') {
        pos
    } else {
        panic!("haven't found start")
    }
}

fn find_target(map: &[Vec<char>]) -> Position {
    if let Some(pos) = find(map, 'E') {
        pos
    } else {
        panic!("haven't found target")
    }
}

#[derive(Debug)]
struct Vertex {
    position: Position,
    distance: usize,
}

impl Vertex {
    fn new(position: Position, distance: usize) -> Self {
        Self { position, distance }
    }
}

fn get_elevation(c: char) -> i32 {
    match c {
        'S' => get_elevation('a'),
        'E' => get_elevation('z'),
        _ => c as i32 - 'a' as i32,
    }
}

fn bfs<F, G>(graph: &[Vec<char>], start: &Position, has_edge: F, is_target: G) -> Option<usize>
where
    F: Fn(&[Vec<char>], &Position, &Position) -> bool,
    G: Fn(&[Vec<char>], &Position) -> bool,
{
    let mut visited: BTreeSet<Position> = BTreeSet::new();
    let mut queue: VecDeque<Vertex> = VecDeque::new();
    visited.insert(*start);
    queue.push_back(Vertex::new(*start, 0));

    while let Some(v) = queue.pop_front() {
        // debug!("Taking {:?} from queue at elevation {}", v, get_elevation(*index(input, &v.position)));

        for (dx, dy) in [(0, 1), (1, 0), (0, -1), (-1, 0)] {
            let d = Position::new(dx, dy);
            let neighbour = v.position + d;

            if in_range(graph, &neighbour)
                && has_edge(graph, &v.position, &neighbour)
                && !visited.contains(&neighbour)
            {
                if is_target(graph, &neighbour) {
                    // debug!("Found target at distance {}", v.distance + 1);
                    return Some(v.distance + 1);
                }

                visited.insert(neighbour);
                queue.push_back(Vertex::new(neighbour, v.distance + 1));
            }
        }
    }

    None
}

struct Day12;
impl Solution<Input, Output> for Day12 {
    fn parse_input<P: AsRef<Path>>(pathname: P) -> Input {
        file_to_lines(pathname)
            .into_iter()
            .map(|line| line.chars().collect_vec())
            .collect_vec()
    }

    fn part_1(input: &Input) -> Output {
        fn has_edge(graph: &[Vec<char>], from: &Position, to: &Position) -> bool {
            get_elevation(graph[*to]) - get_elevation(graph[*from]) <= 1
        }

        let (start, target) = (find_start(input), find_target(input));
        if let Some(distance) = bfs(input, &start, has_edge, |_, &v| v == target) {
            return distance;
        }

        panic!("haven't found path to target")
    }

    fn part_2(input: &Input) -> Output {
        fn has_edge(graph: &[Vec<char>], from: &Position, to: &Position) -> bool {
            (get_elevation(graph[*from]) - get_elevation(graph[*to])) <= 1
        }

        fn is_target(graph: &[Vec<char>], vertex: &Position) -> bool {
            get_elevation(graph[*vertex]) == 0
        }

        let start = find_target(input);
        if let Some(distance) = bfs(input, &start, has_edge, is_target) {
            return distance;
        }

        panic!("haven't found path to target")
    }
}

fn main() -> Result<()> {
    // Day12::run("sample")
    Day12::main()
}

test_sample!(day_12, Day12, 31, 29);
