use std::{collections::HashMap, fmt::Display, str::FromStr};

use aoc_2022::*;

type Input = Vec<Instruction>;
type Output = Out;

#[derive(Debug)]
enum Instruction {
    Addx(i32),
    Noop,
}

impl FromStr for Instruction {
    type Err = Report;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let split_s = s.split_ascii_whitespace().collect_vec();

        match split_s[0] {
            "noop" => Ok(Instruction::Noop),
            "addx" => {
                let value: i32 = split_s[1].parse()?;
                Ok(Instruction::Addx(value))
            }
            _ => Err(eyre!("Invalid instruction {}", split_s[0])),
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
enum Out {
    Strengths(i32),
    Screen(Vec<Vec<char>>),
}

impl Out {
    fn new_strengths() -> Out {
        Out::Strengths(0)
    }

    fn new_screen() -> Out {
        Out::Screen(vec![vec![]])
    }
}

impl Display for Out {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Out::Screen(screen) => write!(
                f,
                "\n{}",
                screen.iter().map(|line| line.iter().join("")).join("\n")
            ),
            Out::Strengths(strengths) => write!(f, "{strengths}"),
        }
    }
}

struct State {
    cycle: i32,
    register: i32,
}

lazy_static! {
    static ref MAPPING: HashMap<bool, char> = HashMap::from([(false, ' '), (true, '█')]);
}

impl State {
    fn new() -> Self {
        Self {
            cycle: 1,
            register: 1,
        }
    }

    fn check_signal_strength(&self, next_cycle: i32, total: &mut i32) {
        if let Some(cycle) = (self.cycle..next_cycle).find(|&c| c >= 20 && (c + 20) % 40 == 0) {
            // debug!(
            //     "Adding {} x {} = {} to sum",
            //     cycle,
            //     self.register,
            //     cycle * self.register
            // );

            *total += cycle * self.register;
        }
    }

    fn draw_crt(&self, next_cycle: i32, screen: &mut Vec<Vec<char>>) {
        // debug!("Checking: {:?} {:?}", i, (self.cycle..next.cycle));
        (self.cycle..next_cycle)
            .map(|c| (c - 1) % 40)
            .for_each(|c| {
                if screen.last().unwrap().len() == 40 {
                    screen.push(vec![]);
                }

                let idx = screen.len() - 1;
                screen[idx].push(MAPPING[&(self.register - 1..=self.register + 1).contains(&c)]);
            });
    }

    fn execute(&self, i: &Instruction, output: &mut Out) -> State {
        let next = match i {
            Instruction::Addx(value) => State {
                cycle: self.cycle + 2,
                register: self.register + value,
            },
            Instruction::Noop => State {
                cycle: self.cycle + 1,
                register: self.register,
            },
        };
        // debug!("New state of CPU: cycle={}, register={}", next.cycle, next.register);

        match output {
            Out::Screen(screen) => self.draw_crt(next.cycle, screen),
            Out::Strengths(strengths) => self.check_signal_strength(next.cycle, strengths),
        }

        next
    }
}

fn evaluate_instructions(instructions: &[Instruction], mut out: Output) -> Output {
    instructions
        .iter()
        .fold(State::new(), |state, instruction| {
            state.execute(instruction, &mut out)
        });

    out
}

struct Day10;
impl Solution<Input, Output> for Day10 {
    fn parse_input<P: AsRef<Path>>(pathname: P) -> Input {
        file_to_structs(pathname)
    }

    fn part_1(input: &Input) -> Output {
        evaluate_instructions(input, Out::new_strengths())
    }

    fn part_2(input: &Input) -> Output {
        evaluate_instructions(input, Out::new_screen())
    }
}

fn main() -> Result<()> {
    Day10::main()
}

test_sample!(
    day_10,
    Day10,
    Out::Strengths(13140),
    Out::Screen(
        ("██  ██  ██  ██  ██  ██  ██  ██  ██  ██  \n\
            ███   ███   ███   ███   ███   ███   ███ \n\
            ████    ████    ████    ████    ████    \n\
            █████     █████     █████     █████     \n\
            ██████      ██████      ██████      ████\n\
            ███████       ███████       ███████     ")
            .lines()
            .map(|l| l.chars().collect_vec())
            .collect_vec()
    )
);
