use std::{cmp::max, collections::BTreeSet};

use aoc_2022::*;

type Input = Vec<Vec<i8>>;
type Output = usize;

type SignedPosition = Vector2D<isize>;
type Position = Vector2D<usize>;
type Visited = BTreeSet<Position>;

fn count_in(trees: &Input, counted: &mut Visited, swap: bool) {
    fn folder(
        trees: &Input,
        counted: &mut Visited,
        mut pos: Position,
        swap: bool,
        tallest: i8,
    ) -> i8 {
        if swap {
            pos = pos.swap();
        }

        if trees[pos] > tallest {
            counted.insert(pos);
        }

        max(tallest, trees[pos])
    }

    (0..trees.len()).for_each(|y| {
        (0..trees[y].len()).fold(-1, |tallest, x| {
            folder(trees, counted, Position::new(x, y), swap, tallest)
        });
        (0..trees[y].len()).rfold(-1, |tallest, x| {
            folder(trees, counted, Position::new(x, y), swap, tallest)
        });
    })
}

fn count_in_rows(trees: &Input, counted: &mut Visited) {
    count_in(trees, counted, false)
}

fn count_in_columns(trees: &Input, counted: &mut Visited) {
    count_in(trees, counted, true)
}

fn count_visible(trees: &Input, position: SignedPosition, diff: SignedPosition) -> usize {
    let max_height = trees[position];
    let mut visible = 0;

    let mut d = 1;
    while in_range(trees, &(position + diff * d)) {
        visible += 1;

        if trees[position + diff * d] >= max_height {
            break;
        }

        d += 1;
    }

    visible as usize
}

fn compute_scenic_score(trees: &Input, x: isize, y: isize) -> usize {
    vec![(0, 1), (1, 0), (0, -1), (-1, 0)]
        .iter()
        .map(|&(dx, dy)| {
            count_visible(
                trees,
                SignedPosition::new(x, y),
                SignedPosition::new(dx, dy),
            )
        })
        .product()
}

struct Day08;
impl Solution<Input, Output> for Day08 {
    fn parse_input<P: AsRef<Path>>(pathname: P) -> Input {
        file_to_string(pathname)
            .lines()
            .map(|line| {
                line.chars()
                    .map(|c| c.to_digit(10).unwrap() as i8)
                    .collect_vec()
            })
            .collect_vec()
    }

    fn part_1(input: &Input) -> Output {
        let mut counted = Visited::new();

        count_in_rows(input, &mut counted);
        count_in_columns(input, &mut counted);

        counted.len()
    }

    fn part_2(input: &Input) -> Output {
        (0..input.len())
            .flat_map(|y| {
                (0..input[y].len())
                    .map(move |x| compute_scenic_score(input, x as isize, y as isize))
            })
            .max()
            .unwrap()
    }
}

fn main() -> Result<()> {
    Day08::main()
}

test_sample!(day_08, Day08, 21, 8);
