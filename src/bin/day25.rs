use std::{char::from_digit, collections::HashMap, fmt::Display, str::FromStr};

use aoc_2022::*;

type Input = Vec<SpecialFuelUnits>;
type Output = String;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
struct SpecialFuelUnits {
    value: i64,
}

impl FromStr for SpecialFuelUnits {
    type Err = Report;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let value = (0..)
            .zip(s.chars().rev())
            .map(|(e, d)| {
                5_i64.pow(e)
                    * match d {
                        '-' => -1,
                        '=' => -2,
                        _ => d.to_digit(10).unwrap().try_into().unwrap(),
                    }
            })
            .sum();

        Ok(Self { value })
    }
}

impl Display for SpecialFuelUnits {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut n = self.value;
        let mut digits: Vec<char> = Vec::new();

        while n > 0 {
            let mut d = n % 5;
            if d > 2 {
                n += 5 - d;
                d -= 5;
            }

            digits.push(match d {
                -1 => '-',
                -2 => '=',
                _ => from_digit(d as u32, 10).unwrap(),
            });

            n /= 5;
        }

        digits.reverse();

        write!(f, "{}", digits.iter().join(""))
    }
}

impl From<i64> for SpecialFuelUnits {
    fn from(value: i64) -> Self {
        Self { value }
    }
}

struct Day25;
impl Solution<Input, Output> for Day25 {
    fn parse_input<P: AsRef<Path>>(pathname: P) -> Input {
        file_to_structs(pathname)
    }

    fn part_1(input: &Input) -> Output {
        SpecialFuelUnits::from(input.iter().map(|s| s.value).sum::<i64>()).to_string()
    }

    fn part_2(_input: &Input) -> Output {
        "Merry Chrysler!!!".to_string()
    }
}

fn main() -> Result<()> {
    // Day25::run("sample")
    Day25::main()
}

test_sample!(
    day_25,
    Day25,
    "2=-1=0".to_string(),
    "Merry Chrysler!!!".to_string()
);

lazy_static! {
    static ref EXAMPLES: HashMap<i64, &'static str> = HashMap::from([
        (1, "1"),
        (2, "2"),
        (3, "1="),
        (4, "1-"),
        (5, "10"),
        (6, "11"),
        (7, "12"),
        (8, "2="),
        (9, "2-"),
        (10, "20"),
        (15, "1=0"),
        (20, "1-0"),
        (2022, "1=11-2"),
        (12345, "1-0---0"),
        (314159265, "1121-1110-1=0"),
        (1747, "1=-0-2"),
        (906, "12111"),
        (198, "2=0="),
        (11, "21"),
        (201, "2=01"),
        (31, "111"),
        (1257, "20012"),
        (32, "112"),
        (353, "1=-1="),
        (107, "1-12"),
        (37, "122"),
        (4890, "2=-1=0"),
    ]);
}

#[cfg(test)]
mod day_25_snafu {
    use super::*;

    #[test]
    fn test_from() {
        for (&n, s) in EXAMPLES.iter() {
            assert_eq!(s.parse::<SpecialFuelUnits>().unwrap().value, n);
        }
    }

    #[test]
    fn test_to() {
        for (&n, s) in EXAMPLES.iter() {
            assert_eq!(SpecialFuelUnits::from(n).to_string(), s.to_string());
        }
    }
}
