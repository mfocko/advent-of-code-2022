use std::{
    cmp::{max, min},
    collections::BTreeMap,
    str::FromStr,
};

use aoc_2022::*;

use rayon::prelude::{IntoParallelIterator, ParallelIterator};

type Input = Vec<Sensor>;
type Output = usize;

type Coord = Vector2D<i32>;

fn manhattan(u: &Coord, v: &Coord) -> i32 {
    let direction = *v - *u;
    (direction.x()).abs() + (direction.y()).abs()
}

#[derive(Debug, PartialEq, Eq, Clone)]
struct Sensor {
    position: Coord,
    beacon: Coord,
}

impl Sensor {
    fn bounds_at_y(&self, y: i32) -> Option<(Coord, Coord)> {
        let m = manhattan(&self.position, &self.beacon);
        if y < self.position.y() - m || y > self.position.y() + m {
            return None;
        }

        let x = self.position.x();
        let side_d = m - (self.position.y() - y).abs();

        Some((Coord::new(x - side_d, y), Coord::new(x + side_d, y)))
    }
}

lazy_static! {
    static ref REGEX: Regex = Regex::new(concat!(
        r"Sensor at x=(?P<sx>-?\d+), y=(?P<sy>-?\d+): ",
        r"closest beacon is at x=(?P<bx>-?\d+), y=(?P<by>-?\d+)"
    ))
    .unwrap();
}

impl FromStr for Sensor {
    type Err = Report;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let caps = REGEX.captures(s).unwrap();
        let (sx, sy, bx, by) = vec!["sx", "sy", "bx", "by"]
            .iter()
            .map(|&p| caps[p].parse::<i32>().unwrap())
            .collect_tuple()
            .unwrap();

        Ok(Sensor {
            position: Coord::new(sx, sy),
            beacon: Coord::new(bx, by),
        })
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
enum Status {
    Sensor,
    Beacon,
    Cannot,
}

fn cannot_contain(sensors: &Input, watched_y: i32) -> Output {
    let mut positions = BTreeMap::<i32, Status>::new();

    // mark unusable positions
    sensors
        .iter()
        .filter_map(|s| s.bounds_at_y(watched_y))
        .for_each(|(l, r)| {
            (l.x()..=r.x()).for_each(|x| {
                positions.insert(x, Status::Cannot);
            })
        });

    // rewrite beacons and sensors if needed
    sensors
        .iter()
        .filter(|s| s.position.y() == watched_y || s.beacon.y() == watched_y)
        .for_each(|s| {
            if s.beacon.y() == watched_y {
                positions.insert(s.beacon.x(), Status::Beacon);
            }

            if s.position.y() == watched_y {
                positions.insert(s.beacon.x(), Status::Sensor);
            }
        });

    // count unusable
    positions
        .iter()
        .filter(|(_, s)| **s == Status::Cannot)
        .count()
}

fn check_row(sensors: &Input, watched_y: i32, upper_bound: i32) -> Vec<(i32, i32)> {
    let mut positions: Vec<(i32, i32)> = Vec::new();

    // mark unusable positions
    positions.extend(
        sensors
            .iter()
            .filter_map(|s| s.bounds_at_y(watched_y))
            .map(|(l, r)| (max(0, l.x()), min(r.x(), upper_bound))),
    );
    positions.sort();

    positions
}

fn find_first_empty(positions: &Vec<(i32, i32)>, upper_bound: i32) -> Option<i32> {
    let mut x = 0;
    for &(from, to) in positions {
        if x >= from && x <= to {
            x = to + 1;
        }
    }

    if x <= upper_bound {
        return Some(x);
    }

    None
}

fn find_distress(sensors: &Input, upper_bound: usize) -> Output {
    let (x, y) = (0..=upper_bound)
        .into_par_iter()
        .find_map_any(|y| {
            let positions = check_row(sensors, y as i32, upper_bound as i32);

            find_first_empty(&positions, upper_bound as i32).map(|x| (x, y))
        })
        .unwrap();

    x as usize * 4000000 + y
}

struct Day15;
impl Solution<Input, Output> for Day15 {
    fn parse_input<P: AsRef<Path>>(pathname: P) -> Input {
        file_to_structs(pathname)
    }

    fn part_1(input: &Input) -> Output {
        cannot_contain(input, 2000000)
    }

    fn part_2(input: &Input) -> Output {
        find_distress(input, 4000000)
    }
}

fn main() -> Result<()> {
    // Day15::run("sample")
    Day15::main()
}

#[cfg(test)]
mod day_15 {
    use super::*;

    #[test]
    fn test_part_1() {
        let sample = Day15::parse_input(&format!("samples/{}.txt", Day15::day()));
        assert_eq!(cannot_contain(&sample, 10), 26);
    }

    #[test]
    fn test_part_2() {
        let sample = Day15::parse_input(&format!("samples/{}.txt", Day15::day()));
        assert_eq!(find_distress(&sample, 20), 56000011);
    }
}

#[cfg(test)]
mod day_15_unit {
    use super::*;

    #[test]
    fn test_manhattan_basic_example() {
        let s = Sensor {
            position: Coord::new(8, 7),
            beacon: Coord::new(2, 10),
        };
        assert_eq!(manhattan(&s.position, &s.beacon), 9);
    }

    #[test]
    fn test_manhattan_close() {
        let s = Sensor {
            position: Coord::new(0, 11),
            beacon: Coord::new(2, 10),
        };
        assert_eq!(manhattan(&s.position, &s.beacon), 3);
    }

    #[test]
    fn test_manhattan_next() {
        let s = Sensor {
            position: Coord::new(2, 11),
            beacon: Coord::new(2, 10),
        };
        assert_eq!(manhattan(&s.position, &s.beacon), 1);
    }

    #[test]
    fn bounds_at_y_basic_example() {
        let s = Sensor {
            position: Coord::new(8, 7),
            beacon: Coord::new(2, 10),
        };

        let bounds = s.bounds_at_y(10);
        assert!(bounds.is_some());

        let (left, right) = bounds.unwrap();
        assert_eq!(left, Coord::new(2, 10));
        assert_eq!(right, Coord::new(14, 10));
    }

    #[test]
    fn bounds_at_y_none() {
        let s = Sensor {
            position: Coord::new(8, 7),
            beacon: Coord::new(2, 10),
        };

        let bounds = s.bounds_at_y(20);
        assert!(bounds.is_none());
    }

    #[test]
    fn bounds_at_y_close_but_not_same() {
        let s = Sensor {
            position: Coord::new(8, 7),
            beacon: Coord::new(9, 7),
        };

        let bounds = s.bounds_at_y(7);
        assert!(bounds.is_some());

        let (left, right) = bounds.unwrap();
        assert_eq!(left, Coord::new(7, 7));
        assert_eq!(right, Coord::new(9, 7));
    }

    #[test]
    fn bounds_at_y_close_and_same() {
        let s = Sensor {
            position: Coord::new(8, 7),
            beacon: Coord::new(9, 7),
        };

        let bounds = s.bounds_at_y(6);
        assert!(bounds.is_some());

        let (left, right) = bounds.unwrap();
        assert_eq!(left, Coord::new(8, 6));
        assert_eq!(right, Coord::new(8, 6));
    }
}
