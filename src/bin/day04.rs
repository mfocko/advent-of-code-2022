use std::ops::RangeInclusive;
use std::str::FromStr;

use aoc_2022::*;

type Input = Vec<Assignment>;
type Output = usize;

struct Assignment(RangeInclusive<i32>, RangeInclusive<i32>);

impl FromStr for Assignment {
    type Err = Report;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let split_s = s.split(',').collect::<Vec<_>>();
        let (left, right) = (split_s[0], split_s[1]);

        let (l_split, r_split) = (
            left.split('-').collect::<Vec<_>>(),
            right.split('-').collect::<Vec<_>>(),
        );
        let (l_min, l_max) = (l_split[0], l_split[1]);
        let (r_min, r_max) = (r_split[0], r_split[1]);

        let (ll, lu) = (l_min.parse::<i32>()?, l_max.parse::<i32>()?);
        let (rl, ru) = (r_min.parse::<i32>()?, r_max.parse::<i32>()?);

        // debug!("Parsed: {}..={}, {}..={}", ll, lu, rl, ru);

        Ok(Assignment(ll..=lu, rl..=ru))
    }
}

impl Assignment {
    fn fully_overlap(&self) -> bool {
        let Assignment(l, r) = self;

        (l.start() <= r.start() && r.end() <= l.end())
            || (r.start() <= l.start() && l.end() <= r.end())
    }

    fn overlap(&self) -> bool {
        let Assignment(l, r) = self;

        (l.contains(r.start()) || l.contains(r.end()))
            || (r.contains(l.start()) || r.contains(l.end()))
    }
}

struct Day04;
impl Solution<Input, Output> for Day04 {
    fn parse_input<P: AsRef<Path>>(pathname: P) -> Input {
        file_to_structs(pathname)
    }

    fn part_1(input: &Input) -> Output {
        input.iter().filter(|a| a.fully_overlap()).count()
    }

    fn part_2(input: &Input) -> Output {
        input.iter().filter(|a| a.overlap()).count()
    }
}

fn main() -> Result<()> {
    Day04::main()
}

test_sample!(day_04, Day04, 2, 4);
