use aoc_2022::*;

type Input = Vec<i32>;
type Output = i32;

fn n_biggest(n: usize, input: &Input) -> Output {
    let mut calories: Vec<i32> = Vec::new();

    let last_elf = input.iter().fold(0, |a, &x| {
        if x == -1 {
            calories.push(a);
            0
        } else {
            a + x
        }
    });
    calories.push(last_elf);

    calories.sort();
    calories.iter().rev().take(n).sum()
}

struct Day01;
impl Solution<Input, Output> for Day01 {
    fn parse_input<P: AsRef<Path>>(pathname: P) -> Input {
        file_to_lines(pathname)
            .iter()
            .map(|s| if s.is_empty() { -1 } else { s.parse().unwrap() })
            .collect()
    }

    fn part_1(input: &Input) -> Output {
        n_biggest(1_usize, input)
    }

    fn part_2(input: &Input) -> Output {
        n_biggest(3_usize, input)
    }
}

fn main() -> Result<()> {
    Day01::main()
}

test_sample!(day_01, Day01, 24000, 45000);
