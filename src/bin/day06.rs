use std::collections::HashSet;

use aoc_2022::*;

type Input = String;
type Output = usize;

fn unique_marker_index(buffer: &Input, n: usize) -> Output {
    let chars: Vec<_> = buffer.chars().collect();

    chars
        .windows(n)
        .enumerate()
        .map(|(i, chars)| (i, chars.iter().collect::<HashSet<_>>()))
        .find_or_first(|(_, s)| s.len() == n)
        .unwrap()
        .0
        + n
}

struct Day06;
impl Solution<Input, Output> for Day06 {
    fn parse_input<P: AsRef<Path>>(pathname: P) -> Input {
        file_to_string(pathname)
    }

    fn part_1(input: &Input) -> Output {
        unique_marker_index(input, 4)
    }

    fn part_2(input: &Input) -> Output {
        unique_marker_index(input, 14)
    }
}

fn main() -> Result<()> {
    Day06::main()
}

test_sample!(day_06, Day06, 7, 19);

#[cfg(test)]
mod day_06_extended {
    use super::*;

    #[test]
    fn test_part_1_example_1() {
        assert_eq!(
            Day06::part_1(&"bvwbjplbgvbhsrlpgdmjqwftvncz".to_string()),
            5
        );
    }

    #[test]
    fn test_part_1_example_2() {
        assert_eq!(
            Day06::part_1(&"nppdvjthqldpwncqszvftbrmjlhg".to_string()),
            6
        );
    }

    #[test]
    fn test_part_1_example_3() {
        assert_eq!(
            Day06::part_1(&"nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg".to_string()),
            10
        );
    }

    #[test]
    fn test_part_1_example_4() {
        assert_eq!(
            Day06::part_1(&"zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw".to_string()),
            11
        );
    }

    #[test]
    fn test_part_2_example_1() {
        assert_eq!(
            Day06::part_2(&"bvwbjplbgvbhsrlpgdmjqwftvncz".to_string()),
            23
        );
    }

    #[test]
    fn test_part_2_example_2() {
        assert_eq!(
            Day06::part_2(&"nppdvjthqldpwncqszvftbrmjlhg".to_string()),
            23
        );
    }

    #[test]
    fn test_part_2_example_3() {
        assert_eq!(
            Day06::part_2(&"nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg".to_string()),
            29
        );
    }

    #[test]
    fn test_part_2_example_4() {
        assert_eq!(
            Day06::part_2(&"zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw".to_string()),
            26
        );
    }
}
