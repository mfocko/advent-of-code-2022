use std::{cmp::Reverse, collections::VecDeque, mem, str::FromStr};

use aoc_2022::*;

type Input = Vec<Monkey>;
type Output = usize;

#[derive(Clone)]
enum Operation {
    Add(usize),
    AddSelf,
    Mul(usize),
    MulSelf,
}

impl Operation {
    fn apply(&self, x: usize) -> usize {
        match self {
            Self::Add(y) => x + y,
            Self::AddSelf => x + x,
            Self::Mul(y) => x * y,
            Self::MulSelf => x * x,
        }
    }
}

impl FromStr for Operation {
    type Err = Report;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref OPERATION_REGEX: Regex =
                Regex::new(r"(?P<l>.*) (?P<op>\+|\*) (?P<r>.*)").unwrap();
        }

        let caps = OPERATION_REGEX.captures(s).unwrap();
        let (l, op, r) = (&caps["l"], &caps["op"], &caps["r"]);

        if l == r {
            match op {
                "+" => Ok(Self::AddSelf),
                "*" => Ok(Self::MulSelf),
                _ => Err(eyre!("invalid operator for ‹old›")),
            }
        } else {
            match op {
                "+" => Ok(Self::Add(r.parse()?)),
                "*" => Ok(Self::Mul(r.parse()?)),
                _ => Err(eyre!("invalid operator for ‹old›")),
            }
        }
    }
}

#[derive(Clone)]
struct Test(usize, usize, usize);
impl Test {
    fn get(&self, worry: usize) -> usize {
        let &Test(div, t, f) = self;

        if worry % div == 0 {
            t
        } else {
            f
        }
    }
}

#[derive(Clone)]
struct Monkey {
    items: Vec<usize>,
    operation: Operation,
    test: Test,

    inspections: usize,
}

impl FromStr for Monkey {
    type Err = Report;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref MONKEY_REGEX: Regex = Regex::new(concat!(
                r"(?s)",
                r"Starting items: (?P<items>[^\n]+).*",
                r"Operation: new = (?P<operation>[^\n]*).*",
                r"Test: divisible by (?P<div>\d+).*",
                r"If true: throw to monkey (?P<if_1>\d+).*",
                r"If false: throw to monkey (?P<if_0>\d+)",
            ))
            .unwrap();
        }

        let caps = MONKEY_REGEX.captures(s).unwrap();
        let items = caps["items"]
            .split(", ")
            .map(|x| x.parse().unwrap())
            .collect_vec();
        let operation = caps["operation"].parse()?;
        let test = Test(
            caps["div"].parse()?,
            caps["if_1"].parse()?,
            caps["if_0"].parse()?,
        );

        Ok(Monkey {
            items,
            operation,
            test,

            inspections: 0,
        })
    }
}

fn round(monkeys: &mut Input, lcm: usize, modular: bool) {
    let mut q: VecDeque<(usize, usize)> = VecDeque::new();

    for i in 0..monkeys.len() {
        let m = &mut monkeys[i];

        for j in 0..m.items.len() {
            let mut worry = m.operation.apply(m.items[j]);
            if modular {
                worry %= lcm;
            } else {
                worry /= 3;
            }

            q.push_back((m.test.get(worry), worry));
            m.inspections += 1;
        }

        m.items.clear();

        while let Some((i, w)) = q.pop_front() {
            monkeys[i].items.push(w);
        }
    }
}

fn euclid(a: usize, b: usize) -> usize {
    // variable names based off euclidean division equation: a = b · q + r
    let (mut a, mut b) = if a > b { (a, b) } else { (b, a) };

    while b != 0 {
        mem::swap(&mut a, &mut b);
        b %= a;
    }

    a
}

fn simulate_rounds(input: &Input, rounds: usize, modular: bool) -> Output {
    let mut monkeys = input.clone();

    let lcm = monkeys
        .iter()
        .map(|m| m.test.0)
        .fold(1, |x, y| (x * y) / euclid(x, y));

    let inspect = vec![
        1, 20, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 1000,
    ];

    for r in 0..rounds {
        round(&mut monkeys, lcm, modular);

        if inspect.contains(&(r + 1)) {
            debug!(
                "Monkeys: {:?}",
                monkeys.iter().map(|m| m.inspections).collect_vec()
            );
        }
    }

    monkeys
        .iter()
        .map(|m| m.inspections)
        // .inspect(|x| debug!("inspections of monkey: {x}"))
        .sorted_by_key(|&i| Reverse(i))
        .take(2)
        .product()
}

struct Day11;
impl Solution<Input, Output> for Day11 {
    fn parse_input<P: AsRef<Path>>(pathname: P) -> Input {
        file_to_string(pathname)
            .split("\n\n")
            .map(|m| m.parse().unwrap())
            .collect_vec()
    }

    fn part_1(input: &Input) -> Output {
        simulate_rounds(input, 20, false)
    }

    fn part_2(input: &Input) -> Output {
        simulate_rounds(input, 10000, true)
    }
}

fn main() -> Result<()> {
    // Day11::run("sample")
    Day11::main()
}

test_sample!(day_11, Day11, 10605, 2713310158);
